﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public GameObject coinPrefab;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start, nb pieces = " + Coin.coinAmount);
    }

    public void AddCoin()
    {
        Debug.Log("########### CLIC BUTTON ############");

        GameObject unObjet = Instantiate(coinPrefab);
        Coin unePiece = unObjet.GetComponent<Coin>();

        Debug.Log("Piece instanciée");
        Debug.Log("numéro = " + unePiece.num);
        Coin.PrintCoinStats();
    }

    public void PrintStats()
    {
        Coin.PrintCoinStats();
    }


}
