﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public static int coinAmount = 0;
    public static int globalNum = 0;
    public int num;

    public void Awake ()
    {
        coinAmount += 1;
        globalNum += 1;
        num = globalNum;

        RandomPosition();
    }

    public void OnMouseDown()
    {
        Debug.Log("########### CLIC COIN ############");
        coinAmount -= 1;
        Debug.Log("Destroy coin number " + num);
        Debug.Log("Remains " + coinAmount + " coins");
        Debug.Log("Next number is " + (globalNum + 1));
        Destroy(gameObject);
    }

    public void RandomPosition()
    {
        transform.position = new Vector2(Random.Range(-7f, 7f), Random.Range(-4f, 4f));
    }

    public static void PrintCoinStats()
    {
        Debug.Log("Next num = " + globalNum + 1);
        Debug.Log("Coin amount = " + coinAmount);
    }
}
